package com.realdolmen.spring.blog;

import com.realdolmen.spring.blog.domain.Author;
import com.realdolmen.spring.blog.domain.Blog;
import com.realdolmen.spring.blog.domain.BlogPost;
import com.realdolmen.spring.blog.repositories.AuthorRepository;
import com.realdolmen.spring.blog.repositories.BlogPostRepository;
import com.realdolmen.spring.blog.repositories.BlogRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

/**
 * Created by cda5732 on 14/04/2015.
 */
@SpringBootApplication
public class ApplicationConfiguration {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(ApplicationConfiguration.class).profiles(
                "production")
                                                                                                             .run();
        // TODO load repositories and test by inserting data into the production database
        AuthorRepository aur = context.getBean(AuthorRepository.class);
        Author a = new Author();
        a.setEmail("hello@test.com");
        a.setFirstName("fn");
        a.setLastName("ln");
        aur.save(a);

        BlogRepository br = context.getBean(BlogRepository.class);
        Blog b = new Blog();
        b.setAuthor(a);
        b.setTitle("myblog");
        br.save(b);

        BlogPostRepository bpr = context.getBean(BlogPostRepository.class);
        BlogPost bp1 = new BlogPost();
        bp1.setBlog(b);
        bp1.setTitle("bp1");
        BlogPost bp2 = new BlogPost();
        bp2.setBlog(b);
        bp2.setTitle("bp2");
        bpr.save(Arrays.asList(bp1, bp2));
    }
}
