package com.realdolmen.spring.blog.repositories;

import com.realdolmen.spring.blog.domain.BlogPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlogPostRepository extends JpaRepository<BlogPost, Long> {
    List<BlogPost> getAllByOrderByCreationDateDesc();
}
