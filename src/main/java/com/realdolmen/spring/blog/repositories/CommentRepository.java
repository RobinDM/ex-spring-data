package com.realdolmen.spring.blog.repositories;

import com.realdolmen.spring.blog.domain.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> getTop50ByBlogPostIdOrderByCreationDateDesc(
            Long blogPostId,
            Pageable pageable
    );
}
