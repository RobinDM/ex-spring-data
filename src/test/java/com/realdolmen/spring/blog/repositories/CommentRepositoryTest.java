package com.realdolmen.spring.blog.repositories;

import com.realdolmen.spring.blog.config.TestConfig;
import com.realdolmen.spring.blog.domain.Comment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfig.class)
// TODO Activate the test profile
@ActiveProfiles("test")
// TODO Activate transactions
@Transactional
public class CommentRepositoryTest {
    @Autowired
    private CommentRepository commentRepository;

    @Test
    public void getTopByBlogPostId4OrderByCreationDateDesc() {
        PageRequest pageRequest = new PageRequest(0, 50);
        Page<Comment> comments = commentRepository.getTop50ByBlogPostIdOrderByCreationDateDesc(4L, pageRequest);
        Assert.assertEquals(50, comments.getSize());
        Assert.assertEquals(10, comments.getNumberOfElements());
    }

    @Test
    public void getTopByBlogPostId7OrderByCreationDateDesc() {
        PageRequest pageRequest = new PageRequest(0, 50);
        Page<Comment> comments = commentRepository.getTop50ByBlogPostIdOrderByCreationDateDesc(7L, pageRequest);
        Assert.assertEquals(0, comments.getNumberOfElements());
    }
}
